//
//  AvatarViewCell.swift
//  emoji
//
//  Created by Joao Fonseca on 17/02/2021.
//  Copyright © 2021 Joao Fonseca. All rights reserved.
//

import SDWebImage
import UIKit

class AvatarViewCell: AbstractImageViewCell {
    static let reuseId = "AvatarViewCell"

    override func loadImage<T>(data: T) where T: Decodable, T: Encodable {
        if let avatar = data as? Avatar {
            if let image = SDImageCache.shared.imageFromCache(forKey: "\(avatar.id)") {
                viewImage.image = image
            } else {
                viewImage.sd_setImage(with: URL(string: avatar.avatar_url), completed: { image, _, _, _ in
                    self.viewImage.image = image
                    SDImageCache.shared.store(image, forKey: "\(avatar.id)", completion: nil)
                })
            }
        }
    }
}
