//
//  AvatarsViewController.swift
//  emoji
//
//  Created by Joao Fonseca on 13/02/2021.
//  Copyright © 2021 Joao Fonseca. All rights reserved.
//

import UIKit

class AvatarsViewController: UIViewController {
    @Inject var viewModel: AvatarsViewModel

    private let itemsPerRow: CGFloat = 4
    private let sectionInsets = UIEdgeInsets(top: 50.0, left: 20.0, bottom: 50.0, right: 20.0)
    private var label: UILabel = {
        let text = UILabel()
        text.translatesAutoresizingMaskIntoConstraints = false
        text.text = .previousAvatars
        text.textColor = UIColor(named: "textColor")
        return text
    }()

    private var collection: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initCollection()
        buildUI()

        viewModel.avatarList.subscribe { _ in
            self.collection.reloadData()
        }
    }

    func buildUI() {
        view.backgroundColor = UIColor(named: "background")
        collection.backgroundColor = .clear
        collection.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(label)
        view.addSubview(collection)

        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 10),
            label.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 10),
            label.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 10),
            collection.topAnchor.constraint(equalTo: label.bottomAnchor, constant: 10),
            collection.leftAnchor.constraint(equalTo: view.leftAnchor),
            collection.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            collection.rightAnchor.constraint(equalTo: view.rightAnchor),
        ])
    }
}

extension AvatarsViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func initCollection() {
        collection = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        collection.register(AvatarViewCell.self, forCellWithReuseIdentifier: AvatarViewCell.reuseId)
        collection.delegate = self
        collection.dataSource = self
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.getAvatarsListCount()
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let avatarCell = collection.dequeueReusableCell(withReuseIdentifier: AvatarViewCell.reuseId, for: indexPath) as! AvatarViewCell
        avatarCell.build()
        avatarCell.loadImage(data: viewModel.getAvatarFor(indexPath.row))

        return avatarCell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        viewModel.deleteAtIndex(indexPath.row)
    }
}

extension AvatarsViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow

        return CGSize(width: widthPerItem, height: widthPerItem)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
}

fileprivate extension String {
    static let previousAvatars = NSLocalizedString("AVATARS_PREVIOUS", comment: "")
}
