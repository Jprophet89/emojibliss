//
//  AvatarsViewModel.swift
//  emoji
//
//  Created by Joao Fonseca on 17/02/2021.
//  Copyright © 2021 Joao Fonseca. All rights reserved.
//

import Foundation
import RxSwift

class AvatarsViewModel {
    @Inject var avatarService: AvatarService
    var avatarList = BehaviorSubject<[Avatar]>(value: [])

    init() {
        avatarList.onNext(getAllAvatar())
    }

    private func getAllAvatar() -> [Avatar] {
        var listResult = avatarService.getAllAvatar()
        listResult.sort(by: { $0.name < $1.name })

        return listResult
    }

    func getAvatarsListCount() -> Int {
        if let count = try? avatarList.value().count {
            return count
        }

        return 0
    }

    func getAvatarFor(_ index: Int) -> Avatar {
        if let list = try? avatarList.value() {
            return list[index]
        }

        return Avatar()
    }

    func deleteAtIndex(_ index: Int) {
        if var list = try? avatarList.value() {
            let avatarToRemove = list[index]
            // remove for DB
            avatarService.removeAvatar(avatarToRemove)
            // remove for the list and update view
            list.remove(at: index)
            avatarList.onNext(list)
        }
    }
}
