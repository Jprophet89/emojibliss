//
//  RepositoriesViewController.swift
//  emoji
//
//  Created by Joao Fonseca on 13/02/2021.
//  Copyright © 2021 Joao Fonseca. All rights reserved.
//

import UIKit

// https://api.github.com/users/apple/repos

class RepositoriesViewController: UIViewController {
    @Inject var viewModel: RepositoriesViewModel

    var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initTable()
        buildUI()

        viewModel.repositoriesTableList.subscribe { _ in
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }

        viewModel.initialize()
    }

    func buildUI() {
        view.backgroundColor = UIColor(named: "background")
        tableView.backgroundColor = .clear
        tableView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(tableView)

        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.leftAnchor.constraint(equalTo: view.leftAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            tableView.rightAnchor.constraint(equalTo: view.rightAnchor),
        ])
    }
}

extension RepositoriesViewController: UITableViewDelegate, UITableViewDataSource {
    func initTable() {
        tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(RepositoryNameCell.self, forCellReuseIdentifier: RepositoryNameCell.reuseId)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getRepositoriesCount()
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: RepositoryNameCell.reuseId) as! RepositoryNameCell

        cell.build(repository: viewModel.getRepositoriesFor(indexPath.row))

        return cell
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if viewModel.hasMoreToFetch() {
            tableView.addLoading(indexPath) {
                self.viewModel.getMoreRepositories()
                tableView.stopLoading()
            }
        } else {
            DispatchQueue.main.async {
                tableView.tableFooterView = nil
            }
        }
    }
}
