//
//  RepositoryNameCell.swift
//  emoji
//
//  Created by Joao Fonseca on 17/02/2021.
//  Copyright © 2021 Joao Fonseca. All rights reserved.
//

import UIKit

class RepositoryNameCell: UITableViewCell {
    static let reuseId = "RepositoryNameCell"
    var label = UILabel()

    func build(repository: Repository) {
        label.translatesAutoresizingMaskIntoConstraints = false
        addSubview(label)

        NSLayoutConstraint.activate([
            label.heightAnchor.constraint(equalToConstant: 40),
            label.topAnchor.constraint(equalTo: topAnchor, constant: 2),
            label.leftAnchor.constraint(equalTo: leftAnchor, constant: 5),
            label.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 2),
            label.rightAnchor.constraint(equalTo: rightAnchor),
        ])

        label.text = repository.name
    }
}
