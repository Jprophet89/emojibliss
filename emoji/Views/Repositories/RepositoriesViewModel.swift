//
//  RepositoriesViewModel.swift
//  emoji
//
//  Created by Joao Fonseca on 17/02/2021.
//  Copyright © 2021 Joao Fonseca. All rights reserved.
//

import Foundation
import RxSwift

class RepositoriesViewModel {
    @Inject var repositoryService: RepositoryService

    var itemsPerLoad = 10
    var indexPage = 1
    var repositoriesTableList = BehaviorSubject<[Repository]>(value: [])
    var repositoriesFullList: [Repository] = []

    func initialize() {
        repositoryService.fetch(completion: { error in
            if !error {
                self.repositoriesFullList = self.repositoryService.getAll()
                self.repositoriesTableList.onNext(self.getRepositoriesElements())
            }
        })
    }

    func getRepositoriesCount() -> Int {
        if let count = try? repositoriesTableList.value().count {
            return count
        }

        return 0
    }

    func getRepositoriesFor(_ index: Int) -> Repository {
        if let list = try? repositoriesTableList.value() {
            return list[index]
        }

        return Repository(id: 0, name: "")
    }

    func hasMoreToFetch() -> Bool {
        return repositoriesFullList.count > getRepositoriesCount()
    }

    func getMoreRepositories() {
        indexPage = indexPage + 1
        repositoriesTableList.onNext(getRepositoriesElements())
    }

    private func getRepositoriesElements() -> [Repository] {
        guard !repositoriesFullList.isEmpty else {
            return []
        }

        if repositoriesFullList.count < indexPage * itemsPerLoad {
            return Array(repositoriesFullList[0 ..< repositoriesFullList.count])
        } else {
            return Array(repositoriesFullList[0 ..< indexPage * itemsPerLoad])
        }
    }
}
