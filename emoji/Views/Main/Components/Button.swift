//
//  Button.swift
//  emoji
//
//  Created by Joao Fonseca on 13/02/2021.
//  Copyright © 2021 Joao Fonseca. All rights reserved.
//

import UIKit

class Button: UIButton {
    init(title: String) {
        super.init(frame: .infinite)
        setTitle(title, for: .normal)
        backgroundColor = UIColor(named: "buttonBackground")
        tintColor = UIColor(named: "background")
        layer.cornerRadius = 5
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
