//
//  SearchAvatar.swift
//  emoji
//
//  Created by Joao Fonseca on 17/02/2021.
//  Copyright © 2021 Joao Fonseca. All rights reserved.
//

import UIKit

class SearchAvatar: UIView {
    private var stackView = UIStackView()
    private var textView = UITextField()
    private var clearTextButton = UIButton()
    public let searchButton = Button(title: .search)

    func build() {
        stackView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(stackView)
        stackView.axis = .horizontal
        stackView.spacing = 5.0
        stackView.alignment = .fill
        stackView.addArrangedSubview(textView)
        stackView.addArrangedSubview(searchButton)

        clearTextButton.setImage(UIImage(named: "clear"), for: .normal)
        clearTextButton.contentEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
        clearTextButton.frame = CGRect(x: 0, y: 0, width: 15, height: textView.frame.height)
        clearTextButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(clearText)))

        textView.placeholder = .placeholder
        textView.layer.borderWidth = 1
        textView.layer.cornerRadius = 5
        textView.layer.borderColor = UIColor(named: "buttonBackground")?.cgColor
        textView.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: textView.frame.height))
        textView.leftViewMode = .always
        textView.rightView = clearTextButton
        textView.rightViewMode = .whileEditing

        searchButton.contentEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)

        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: topAnchor),
            stackView.leftAnchor.constraint(equalTo: leftAnchor),
            stackView.bottomAnchor.constraint(equalTo: bottomAnchor),
            stackView.rightAnchor.constraint(equalTo: rightAnchor),
        ])
    }

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        textView.layer.borderColor = UIColor(named: "buttonBackground")?.cgColor
    }

    public func getText() -> String {
        return textView.text ?? ""
    }

    @objc private func clearText() {
        textView.text = ""
    }
}

fileprivate extension String {
    static let search = NSLocalizedString("MAIN_SEARCH", comment: "")
    static let placeholder = NSLocalizedString("MAIN_SEARCH_PLACEHOLDER", comment: "")
}
