//
//  MainViewModel.swift
//  emoji
//
//  Created by Joao Fonseca on 13/02/2021.
//  Copyright © 2021 Joao Fonseca. All rights reserved.
//

import RxSwift
import SDWebImage
import UIKit

class MainViewModel {
    @Inject var emojiService: EmojiService
    @Inject var avatarService: AvatarService

    var emojiSelected = PublishSubject<Emoji?>()
    var avatarSelected = PublishSubject<Avatar?>()

    weak var delegate: MainViewAlert?

    init() {
        emojiSelected.onNext(nil)
        avatarSelected.onNext(nil)
    }

    func getRandomAvatar() {
        if let emoji = emojiService.getRandomEmoji() {
            emojiSelected.onNext(emoji)
        }
    }

    func searchForAvatar(_ text: String) {
        avatarService.searchForAvatar(text: text) { avatar, error in
            if error {
                self.delegate?.alert(message: .userNotFound)
                return
            }

            self.avatarSelected.onNext(avatar)
        }
    }
}

fileprivate extension String {
    static let userNotFound = NSLocalizedString("USER_NOT_FOUND", comment: "")
}
