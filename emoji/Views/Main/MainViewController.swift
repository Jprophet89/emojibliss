//
//  MainViewController.swift
//  emoji
//
//  Created by Joao Fonseca on 12/02/2021.
//  Copyright © 2021 Joao Fonseca. All rights reserved.
//

import SDWebImage
import Swinject
import UIKit

protocol MainViewAlert: AnyObject {
    func alert(message: String)
}

class MainViewController: UIViewController {
    @Inject var navigator: MainNavigator
    @Inject var viewModel: MainViewModel

    public let stackView = UIStackView()
    public var quickImageView = UIImageView()
    public var randomEmojiBT = Button(title: .randomEmoji)
    public var emojiListBT = Button(title: .emojiList)
    public var searchAvatarView = SearchAvatar()
    public var avatarsBT = Button(title: .avatars)
    public var appleRepoBT = Button(title: .appleRepo)

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        buildUI()
        viewModel.delegate = self
        viewModel.emojiSelected.subscribe(onNext: { emoji in
            guard let emoji = emoji else {
                return
            }

            if let image = SDImageCache.shared.imageFromCache(forKey: emoji.id) {
                self.quickImageView.image = image
            } else {
                self.quickImageView.sd_setImage(with: URL(string: emoji.url), completed: { image, _, _, _ in
                    self.quickImageView.image = image
                    SDImageCache.shared.store(image, forKey: emoji.id, completion: nil)
                })
            }
        })

        viewModel.avatarSelected.subscribe(onNext: { avatar in
            guard let avatar = avatar else {
                return
            }

            if let image = SDImageCache.shared.imageFromCache(forKey: "\(avatar.id)") {
                self.quickImageView.image = image
            } else {
                self.quickImageView.sd_setImage(with: URL(string: avatar.avatar_url), completed: { image, _, _, _ in
                    self.quickImageView.image = image
                    SDImageCache.shared.store(image, forKey: "\(avatar.id)", completion: nil)
                })
            }
        })
    }

    @objc func getRandomAvatar() {
        viewModel.getRandomAvatar()
    }

    @objc func goToEmojiList() {
        navigator.presentEmoji(vc: self)
    }

    @objc func goToAvatars() {
        navigator.presentAvatars(vc: self)
    }

    @objc func goToAppleRepositories() {
        navigator.presentRepositores(vc: self)
    }

    func buildUI() {
        view.backgroundColor = UIColor(named: "background")

        stackView.axis = .vertical
        stackView.distribution = .equalCentering
        stackView.spacing = 5.0
        stackView.alignment = .fill
        stackView.translatesAutoresizingMaskIntoConstraints = false
        searchAvatarView.build()
        searchAvatarView.searchButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(searchAvatar)))
        view.addSubview(stackView)

        quickImageView.contentMode = .scaleAspectFit
        stackView.addArrangedSubview(quickImageView)
        stackView.addArrangedSubview(randomEmojiBT)
        stackView.addArrangedSubview(emojiListBT)
        stackView.addArrangedSubview(searchAvatarView)
        stackView.addArrangedSubview(avatarsBT)
        stackView.addArrangedSubview(appleRepoBT)

        NSLayoutConstraint.activate([
            stackView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            stackView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            stackView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 25),
            stackView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -25),
            quickImageView.heightAnchor.constraint(equalToConstant: 90),
            randomEmojiBT.heightAnchor.constraint(equalToConstant: 40),
            emojiListBT.heightAnchor.constraint(equalToConstant: 40),
            searchAvatarView.heightAnchor.constraint(equalToConstant: 40),
            avatarsBT.heightAnchor.constraint(equalToConstant: 40),
            appleRepoBT.heightAnchor.constraint(equalToConstant: 40),
        ])

        randomEmojiBT.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(getRandomAvatar)))
        emojiListBT.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goToEmojiList)))
        avatarsBT.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goToAvatars)))
        appleRepoBT.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goToAppleRepositories)))
    }

    @objc func searchAvatar() {
        viewModel.searchForAvatar(searchAvatarView.getText())
    }
}

extension MainViewController: MainViewAlert {
    func alert(message: String) {
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
            alert.dismiss(animated: true, completion: nil)
        }))
        present(alert, animated: true, completion: nil)
    }
}

fileprivate extension String {
    static let randomEmoji = NSLocalizedString("MAIN_RANDOM_EMOJI", comment: "")
    static let avatars = NSLocalizedString("MAIN_AVATARS_LIST", comment: "")
    static let emojiList = NSLocalizedString("MAIN_EMOJI_LIST", comment: "")
    static let appleRepo = NSLocalizedString("MAIN_APPLE_REPO", comment: "")
    static let search = NSLocalizedString("MAIN_SEARCH", comment: "")
}
