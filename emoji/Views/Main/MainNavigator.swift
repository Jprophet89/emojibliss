//
//  MainNavigator.swift
//  emoji
//
//  Created by Joao Fonseca on 13/02/2021.
//  Copyright © 2021 Joao Fonseca. All rights reserved.
//

import UIKit

class MainNavigator {
    func presentEmoji(vc: UIViewController?) {
        guard let mainVC = vc as? MainViewController else {
            return
        }

        let emojiVC = EmojiListViewController()
        mainVC.navigationController?.isNavigationBarHidden = false
        mainVC.navigationController?.navigationBar.tintColor = UIColor(named: "buttonBackground")
        mainVC.navigationController?.pushViewController(emojiVC, animated: true)
    }

    func presentAvatars(vc: UIViewController?) {
        guard let mainVC = vc as? MainViewController else {
            return
        }

        let avatarsVC = AvatarsViewController()
        mainVC.navigationController?.isNavigationBarHidden = false
        mainVC.navigationController?.pushViewController(avatarsVC, animated: true)
    }

    func presentRepositores(vc: UIViewController?) {
        guard let mainVC = vc as? MainViewController else {
            return
        }

        let repositoriesVC = RepositoriesViewController()
        mainVC.navigationController?.isNavigationBarHidden = false
        mainVC.navigationController?.pushViewController(repositoriesVC, animated: true)
    }
}
