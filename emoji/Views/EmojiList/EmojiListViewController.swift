//
//  EmojiListViewController.swift
//  emoji
//
//  Created by Joao Fonseca on 13/02/2021.
//  Copyright © 2021 Joao Fonseca. All rights reserved.
//

import UIKit

class EmojiListViewController: UIViewController {
    @Inject var viewModel: EmojiListViewModel

    private let itemsPerRow: CGFloat = 4
    private let sectionInsets = UIEdgeInsets(top: 50.0, left: 20.0, bottom: 50.0, right: 20.0)
    private var collection: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initCollection()
        buildUI()

        viewModel.emojiList.subscribe { _ in
            self.collection.reloadData()
            self.collection.refreshControl?.endRefreshing()
        }
    }

    func buildUI() {
        view.backgroundColor = UIColor(named: "background")
        collection.backgroundColor = .clear
        collection.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(collection)

        NSLayoutConstraint.activate([
            collection.topAnchor.constraint(equalTo: view.topAnchor),
            collection.leftAnchor.constraint(equalTo: view.leftAnchor),
            collection.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            collection.rightAnchor.constraint(equalTo: view.rightAnchor),
        ])
    }
}

extension EmojiListViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func initCollection() {
        collection = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        collection.register(EmojiViewCell.self, forCellWithReuseIdentifier: EmojiViewCell.reuseId)
        collection.delegate = self
        collection.dataSource = self

        let refresh = UIRefreshControl()
        refresh.tintColor = UIColor(named: "loadingRed")
        refresh.addTarget(self, action: #selector(resetList), for: .valueChanged)
        collection.refreshControl = refresh
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.getEmojiListCount()
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let emojiCell = collection.dequeueReusableCell(withReuseIdentifier: EmojiViewCell.reuseId, for: indexPath) as! EmojiViewCell
        emojiCell.build()
        emojiCell.loadImage(data: viewModel.getEmojiFor(indexPath.row))

        return emojiCell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        viewModel.deleteAtIndex(indexPath.row)
    }

    @objc func resetList() {
        viewModel.resetListFromMemory()
    }
}

extension EmojiListViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow

        return CGSize(width: widthPerItem, height: widthPerItem)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
}
