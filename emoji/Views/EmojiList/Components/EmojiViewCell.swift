//
//  EmojiViewCell.swift
//  emoji
//
//  Created by Joao Fonseca on 17/02/2021.
//  Copyright © 2021 Joao Fonseca. All rights reserved.
//

import SDWebImage
import UIKit

class EmojiViewCell: AbstractImageViewCell {
    static let reuseId = "EmojiViewCell"

    override func loadImage<T>(data: T) where T: Decodable, T: Encodable {
        if let emoji = data as? Emoji {
            if let image = SDImageCache.shared.imageFromCache(forKey: emoji.id) {
                viewImage.image = image
            } else {
                viewImage.sd_setImage(with: URL(string: emoji.url), completed: { image, _, _, _ in
                    self.viewImage.image = image
                    SDImageCache.shared.store(image, forKey: emoji.id, completion: nil)
                })
            }
        }
    }
}
