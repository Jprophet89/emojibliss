//
//  EmojiListViewModel.swift
//  emoji
//
//  Created by Joao Fonseca on 17/02/2021.
//  Copyright © 2021 Joao Fonseca. All rights reserved.
//

import Foundation
import RxSwift

class EmojiListViewModel {
    @Inject var emojiService: EmojiService

    var emojiList = BehaviorSubject<[Emoji]>(value: [])

    init() {
        emojiList.onNext(getAllEmojis())
    }

    func getAllEmojis() -> [Emoji] {
        var listResult: Array<Emoji> = []
        let storeList = emojiService.getAllEmojis()
        for (key, value) in storeList {
            if let id = key as? String, let url = value as? String {
                listResult.append(Emoji(id: id, url: url))
            }
        }

        listResult.sort(by: { $0.id < $1.id })

        return listResult
    }

    func getEmojiListCount() -> Int {
        if let count = try? emojiList.value().count {
            return count
        }

        return 0
    }

    func getEmojiFor(_ index: Int) -> Emoji {
        if let list = try? emojiList.value() {
            return list[index]
        }

        return Emoji(id: "", url: "")
    }

    func deleteAtIndex(_ index: Int) {
        if var list = try? emojiList.value() {
            list.remove(at: index)
            emojiList.onNext(list)
        }
    }

    func resetListFromMemory() {
        emojiList.onNext(getAllEmojis())
    }
}
