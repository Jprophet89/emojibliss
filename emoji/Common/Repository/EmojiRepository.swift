//
//  EmojiRepository.swift
//  emoji
//
//  Created by Joao Fonseca on 15/02/2021.
//  Copyright © 2021 Joao Fonseca. All rights reserved.
//

import Foundation
import SDWebImage

fileprivate let EMOJI_HASH = "EMOJI_HASH"

class EmojiRepository {
    private var queue = DispatchQueue(label: "emojiImageQueue")

    static var shared: EmojiRepository = EmojiRepository()
    var emojis: [AnyHashable: Any] = [:]

    func updateEmojis(_ data: [AnyHashable: Any]) {
        emojis = getEmojiList()

        for (key, value) in data {
            emojis[key] = value

            queue.async {
                if let urlKey = key as? String,
                   let urlString = value as? String,
                   let url = URL(string: urlString),
                   let data = try? Data(contentsOf: url) {
                    SDImageCache.shared.store(UIImage(data: data as Data), forKey: urlKey, completion: nil)
                }
            }
        }

        setEmojiList(emojis)
    }

    func getEmojis() -> [AnyHashable: Any] {
        return getEmojiList()
    }

    private func getEmojiList() -> [AnyHashable: Any] {
        if let emojiData = UserDefaults.standard.object(forKey: EMOJI_HASH) as? Data {
            return NSKeyedUnarchiver.unarchiveObject(with: emojiData) as? [AnyHashable: Any] ?? [:]
        }

        return [:]
    }

    private func setEmojiList(_ data: [AnyHashable: Any]) {
        let emojiData = NSKeyedArchiver.archivedData(withRootObject: data)
        UserDefaults.standard.set(emojiData, forKey: EMOJI_HASH)
    }
}
