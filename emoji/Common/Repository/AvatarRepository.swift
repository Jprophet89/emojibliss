//
//  AvatarRepository.swift
//  emoji
//
//  Created by Joao Fonseca on 17/02/2021.
//  Copyright © 2021 Joao Fonseca. All rights reserved.
//

import Foundation

fileprivate let AVATAR_CONTENT = "AVATAR_CONTENT"

class AvatarRepository {
    static func get() -> [Avatar] {
        do {
            if let encodedObject = UserDefaults.standard.object(forKey: AVATAR_CONTENT) as? Data {
                let decoder = JSONDecoder()
                let result = try decoder.decode(Array<Avatar>.self, from: encodedObject)
                return result
            }
        } catch {}

        return []
    }

    static func add(_ avatar: Avatar) {
        var data = AvatarRepository.get()
        data.append(avatar)

        do {
            let encoder = JSONEncoder()
            let json = try encoder.encode(data)

            UserDefaults.standard.set(json, forKey: AVATAR_CONTENT)
            UserDefaults.standard.synchronize()
        } catch {
        }
    }

    static func remove(_ avatar: Avatar) {
        var data = AvatarRepository.get()
        data.removeAll(where: { $0.id == avatar.id })

        do {
            let encoder = JSONEncoder()
            let json = try encoder.encode(data)

            UserDefaults.standard.set(json, forKey: AVATAR_CONTENT)
            UserDefaults.standard.synchronize()
        } catch {
        }
    }
}
