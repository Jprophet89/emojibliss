//
//  RepositoriesRepository.swift
//  emoji
//
//  Created by Joao Fonseca on 17/02/2021.
//  Copyright © 2021 Joao Fonseca. All rights reserved.
//

import Foundation

fileprivate let REPOSITORIES_HASH = "REPOSITORIES_HASH"

class RepositoriesRepository {
    static func get() -> [Repository] {
        do {
            if let encodedObject = UserDefaults.standard.object(forKey: REPOSITORIES_HASH) as? Data {
                let decoder = JSONDecoder()
                let result = try decoder.decode(Array<Repository>.self, from: encodedObject)
                return result
            }
        } catch {}

        return []
    }

    static func add(_ repository: [Repository]) {
        do {
            let encoder = JSONEncoder()
            let json = try encoder.encode(repository)

            UserDefaults.standard.set(json, forKey: REPOSITORIES_HASH)
            UserDefaults.standard.synchronize()
        } catch {
        }
    }
}
