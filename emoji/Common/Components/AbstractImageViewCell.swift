//
//  AbstractImageViewCell.swift
//  emoji
//
//  Created by Joao Fonseca on 17/02/2021.
//  Copyright © 2021 Joao Fonseca. All rights reserved.
//

import Foundation
import UIKit

class AbstractImageViewCell: UICollectionViewCell {
    var viewImage = UIImageView()

    func build() {
        backgroundColor = .clear
        viewImage.translatesAutoresizingMaskIntoConstraints = false
        viewImage.contentMode = .scaleAspectFit
        addSubview(viewImage)

        NSLayoutConstraint.activate([
            viewImage.topAnchor.constraint(equalTo: topAnchor),
            viewImage.leftAnchor.constraint(equalTo: leftAnchor),
            viewImage.bottomAnchor.constraint(equalTo: bottomAnchor),
            viewImage.rightAnchor.constraint(equalTo: rightAnchor),
        ])
    }

    open func loadImage<T>(data: T) where T: Codable {}
}
