//
//  UITableView+Pagination.swift
//  emoji
//
//  Created by Joao Fonseca on 19/02/2021.
//  Copyright © 2021 Joao Fonseca. All rights reserved.
//

import UIKit

extension UITableView {
    func indicatorView() -> UIActivityIndicatorView {
        var activityIndicatorView = UIActivityIndicatorView()
        if tableFooterView == nil {
            let indicatorFrame = CGRect(x: 0, y: 0, width: bounds.width, height: 40)
            activityIndicatorView = UIActivityIndicatorView(frame: indicatorFrame)
            activityIndicatorView.isHidden = false
            activityIndicatorView.autoresizingMask = [.flexibleLeftMargin, .flexibleRightMargin]
            activityIndicatorView.isHidden = true
            tableFooterView = activityIndicatorView
            return activityIndicatorView
        } else {
            return activityIndicatorView
        }
    }

    func addLoading(_ indexPath: IndexPath, closure: @escaping (() -> Void)) {
        indicatorView().startAnimating()
        if let lastVisibleIndexPath = indexPathsForVisibleRows?.last {
            if indexPath == lastVisibleIndexPath && indexPath.row == numberOfRows(inSection: 0) - 1 {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    closure()
                }
            }
        }
        indicatorView().isHidden = false
    }

    func stopLoading() {
        indicatorView().stopAnimating()
        indicatorView().isHidden = true
    }
}
