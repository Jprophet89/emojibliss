//
//  DependencyInjection.swift
//  emoji
//
//  Created by Joao Fonseca on 13/02/2021.
//  Copyright © 2021 Joao Fonseca. All rights reserved.
//

import Foundation
import Swinject
import UIKit

@propertyWrapper
public struct Inject<Service> {
    private var service: Service

    public init() {
        service = DependencyInjection.sharedContainer().resolve(Service.self)!
    }

    public var wrappedValue: Service {
        get { return service }
        mutating set { service = newValue }
    }

    public var projectedValue: Inject<Service> { self }
}

open class DependencyInjection: NSObject {
    // MARK: - Container

    private let container = Container()
    private var dependenciesLoaded: Bool = false

    open func getContainer() -> Container {
        return container
    }

    override public init() {
        super.init()
        loadDependencies()
    }

    func loadDependencies() {
        loadMainDependencies()
        loadRepositoryDependencies()
        loadEmojiListDependencies()
        loadAvatarsListDependencies()
        loadServicesDependencies()
    }

    func loadMainDependencies() {
        container.register(MainNavigator.self) { _ in MainNavigator() }
        container.register(MainViewModel.self) { _ in MainViewModel() }
    }

    func loadAvatarsListDependencies() {
        container.register(AvatarsViewModel.self) { _ in AvatarsViewModel() }
    }

    func loadEmojiListDependencies() {
        container.register(EmojiListViewModel.self) { _ in EmojiListViewModel() }
    }

    func loadRepositoryDependencies() {
        container.register(RepositoriesViewModel.self) { _ in RepositoriesViewModel() }
    }

    func loadServicesDependencies() {
        container.register(EmojiService.self) { _ in EmojiServiceImpl() }
        container.register(AvatarService.self) { _ in AvatarServiceImpl() }
        container.register(RepositoryService.self) { _ in RepositoryServiceImpl() }
    }

    public static func sharedContainer() -> Container {
        return (UIApplication.shared.delegate as! AppDelegate).dependencyInjection.getContainer()
    }
}
