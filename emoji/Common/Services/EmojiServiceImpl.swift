//
//  EmojiServiceImpl.swift
//  emoji
//
//  Created by Joao Fonseca on 15/02/2021.
//  Copyright © 2021 Joao Fonseca. All rights reserved.
//

import Alamofire
import Foundation

class EmojiServiceImpl: EmojiService {
    func getAllEmojis() -> [AnyHashable: Any] {
        return EmojiRepository.shared.getEmojis()
    }

    func getRandomEmoji() -> Emoji? {
        let emojisList = EmojiRepository.shared.getEmojis()
        if let keySelected = emojisList.keys.randomElement(),
           let id = keySelected as? String,
           let urlString = emojisList[keySelected] as? String {
            return Emoji(id: id, url: urlString)
        }

        return nil
    }

    func fetchEmojis() {
        AF.request("https://api.github.com/emojis", method: .get).responseJSON { response in
            switch response.result {
                case let .success(data):
                    if let data = data as? [AnyHashable: Any] {
                        EmojiRepository.shared.updateEmojis(data)
                    }
                case .failure:
                    break
            }
        }
    }
}
