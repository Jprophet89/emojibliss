//
//  RepositoryService.swift
//  emoji
//
//  Created by Joao Fonseca on 19/02/2021.
//  Copyright © 2021 Joao Fonseca. All rights reserved.
//

import Foundation

protocol RepositoryService {
    func fetch(completion: @escaping (_ error: Bool) -> Void)
    func getAll() -> [Repository]
}
