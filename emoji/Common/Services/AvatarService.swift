//
//  AvatarService.swift
//  emoji
//
//  Created by Joao Fonseca on 17/02/2021.
//  Copyright © 2021 Joao Fonseca. All rights reserved.
//

import Foundation

protocol AvatarService {
    func getAllAvatar() -> [Avatar]
    func searchForAvatar(text: String, completion: @escaping (_ avatar: Avatar?, _ error: Bool) -> Void)
    func removeAvatar(_ avatar: Avatar)
}
