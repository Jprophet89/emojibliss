//
//  RepositoryServiceImpl.swift
//  emoji
//
//  Created by Joao Fonseca on 19/02/2021.
//  Copyright © 2021 Joao Fonseca. All rights reserved.
//

import Alamofire
import Foundation

class RepositoryServiceImpl: RepositoryService {
    func fetch(completion: @escaping (_ error: Bool) -> Void) {
        AF.request("https://api.github.com/users/apple/repos", method: .get).response { response in
            switch response.result {
                case let .success(data):
                    guard let data = data else {
                        completion(true)
                        return
                    }

                    do {
                        let jsonDecoder = JSONDecoder()
                        let repositories = try jsonDecoder.decode(Array<Repository>.self, from: data)
                        RepositoriesRepository.add(repositories)
                        completion(false)
                    } catch {
                    }
                case .failure:
                    completion(true)
            }
        }
    }

    func getAll() -> [Repository] {
        return RepositoriesRepository.get()
    }
}
