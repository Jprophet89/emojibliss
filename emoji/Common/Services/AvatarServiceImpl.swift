//
//  AvatarServiceImpl.swift
//  emoji
//
//  Created by Joao Fonseca on 17/02/2021.
//  Copyright © 2021 Joao Fonseca. All rights reserved.
//

import Alamofire
import Foundation

class AvatarServiceImpl: AvatarService {
    func getAllAvatar() -> [Avatar] {
        return AvatarRepository.get()
    }

    func searchForAvatar(text: String, completion: @escaping (_ avatar: Avatar?, _ error: Bool) -> Void) {
        let listOnMemory = AvatarRepository.get()

        for avatar in listOnMemory {
            if avatar.has(content: text) {
                completion(avatar, false)
                return
            }
        }

        AF.request("https://api.github.com/users/\(text)", method: .get).response { response in
            switch response.result {
                case let .success(data):
                    guard let data = data else {
                        completion(nil, true)
                        return
                    }

                    do {
                        let jsonDecoder = JSONDecoder()
                        let avatar = try jsonDecoder.decode(Avatar.self, from: data)
                        AvatarRepository.add(avatar)
                        completion(avatar, false)
                    } catch {
                        completion(nil, true)
                    }
                case .failure:
                    completion(nil, true)
            }
        }
    }

    func removeAvatar(_ avatar: Avatar) {
        AvatarRepository.remove(avatar)
    }
}
