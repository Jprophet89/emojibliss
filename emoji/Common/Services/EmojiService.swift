//
//  EmojiService.swift
//  emoji
//
//  Created by Joao Fonseca on 15/02/2021.
//  Copyright © 2021 Joao Fonseca. All rights reserved.
//

import Foundation

protocol EmojiService {
    func fetchEmojis()
    func getAllEmojis() -> [AnyHashable: Any]
    func getRandomEmoji() -> Emoji?
}
