//
//  Avatar.swift
//  emoji
//
//  Created by Joao Fonseca on 17/02/2021.
//  Copyright © 2021 Joao Fonseca. All rights reserved.
//

import Foundation

struct Avatar: Codable {
    var login: String
    var id: Int
    var node_id: String
    var avatar_url: String
    var gravatar_id: String
    var url: String
    var html_url: String
    var followers_url: String
    var following_url: String
    var gists_url: String
    var starred_url: String
    var subscriptions_url: String
    var organizations_url: String
    var repos_url: String
    var events_url: String
    var received_events_url: String
    var type: String
    var name: String
    var blog: String
    var location: String

    init() {
        login = ""
        id = 0
        node_id = ""
        avatar_url = ""
        gravatar_id = ""
        url = ""
        html_url = ""
        followers_url = ""
        following_url = ""
        gists_url = ""
        starred_url = ""
        subscriptions_url = ""
        organizations_url = ""
        repos_url = ""
        events_url = ""
        received_events_url = ""
        type = ""
        name = ""
        blog = ""
        location = ""
    }

    func has(content: String) -> Bool {
        return login.contains(content) ||
            node_id.contains(content) ||
            avatar_url.contains(content) ||
            gravatar_id.contains(content) ||
            url.contains(content) ||
            html_url.contains(content) ||
            followers_url.contains(content) ||
            following_url.contains(content) ||
            gists_url.contains(content) ||
            starred_url.contains(content) ||
            subscriptions_url.contains(content) ||
            organizations_url.contains(content) ||
            repos_url.contains(content) ||
            events_url.contains(content) ||
            received_events_url.contains(content) ||
            type.contains(content) ||
            name.contains(content) ||
            blog.contains(content) ||
            location.contains(content)
    }
}
