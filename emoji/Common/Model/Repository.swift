//
//  Repository.swift
//  emoji
//
//  Created by Joao Fonseca on 17/02/2021.
//  Copyright © 2021 Joao Fonseca. All rights reserved.
//

import Foundation

struct Repository: Codable {
    var id: Int
    var node_id: String
    var name: String
    var full_name: String

    init(id: Int, name: String) {
        self.id = 0
        node_id = ""
        self.name = name
        full_name = ""
    }
}
