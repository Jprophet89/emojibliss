//
//  Emoji.swift
//  emoji
//
//  Created by Joao Fonseca on 15/02/2021.
//  Copyright © 2021 Joao Fonseca. All rights reserved.
//

import Foundation

struct Emoji: Codable {
    var id: String
    var url: String

    init(id: String, url: String) {
        self.id = id
        self.url = url
    }
}
