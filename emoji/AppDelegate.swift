//
//  AppDelegate.swift
//  emoji
//
//  Created by Joao Fonseca on 12/02/2021.
//  Copyright © 2021 Joao Fonseca. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    private var _dependencyInjection: DependencyInjection?
    public var dependencyInjection: DependencyInjection {
        if _dependencyInjection == nil {
            _dependencyInjection = DependencyInjection()
        }
        return _dependencyInjection!
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        window = UIWindow(frame: UIScreen.main.bounds)
        let navigator = UINavigationController(rootViewController: MainViewController())
        navigator.navigationBar.tintColor = UIColor(named: "buttonBackground")
        window?.rootViewController = navigator
        window?.makeKeyAndVisible()

        if let emojiService: EmojiService = dependencyInjection.getContainer().resolve(EmojiService.self) {
            emojiService.fetchEmojis()
        }

        return true
    }

    // MARK: UISceneSession Lifecycle

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
}
